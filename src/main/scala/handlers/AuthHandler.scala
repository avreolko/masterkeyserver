package handlers

import com.google.gson.Gson
import schema.MasterKey
import helpers.Utils
import helpers.auth.SimpleAuthManager
import org.squeryl.PrimitiveTypeMode._
import spray.http._
import spray.http.StatusCodes
import spray.http.ContentTypes

import spray.routing._
/**
  * Created by Valentin on 26/01/17.
  */

class AuthRequest(val login: String, val pass: String)
class SimpleAuthRequest(val pass: String)

class AuthHandler(val json : String = "") extends BaseHandler {
  def process : (HttpResponse) = {
    val request = gson.fromJson(json, classOf[SimpleAuthRequest])
    auth(request.pass)
  }

  def auth(pass : String) : (HttpResponse) = {
    var allOk = false

    val authToken = java.util.UUID.randomUUID.toString

    transaction{
      MasterKey.settings.where(s => s.entity === "simplePassHash").headOption match {
        case Some(s) => if (s.value == Utils.md5(pass)){
          SimpleAuthManager.addMasterToken(authToken)
          allOk = true
        }
        case None => allOk = false
      }
    }

    if (allOk){
      val header = HttpHeaders.`Set-Cookie`(HttpCookie("authToken", authToken))
      HttpResponse(StatusCodes.OK, "all ok!").withHeaders(header)
    } else HttpResponse(StatusCodes.Unauthorized, "wrong pass")
  }
}
