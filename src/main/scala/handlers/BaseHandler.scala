package handlers

import com.google.gson.{ExclusionStrategy, FieldAttributes, Gson, GsonBuilder}
import db.DBSession
import spray.http.HttpResponse

/**
  * Created by Valentin on 26/01/17.
  */


trait JsonProcessor {
  val gson = new Gson()
}

abstract class BaseHandler extends AbstractHandler with JsonProcessor with DBSession {
  def process : (HttpResponse)
  def requiresAuth : Boolean = false
}
