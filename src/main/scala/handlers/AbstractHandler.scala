package handlers

import spray.http.HttpResponse

/**
 * Created by Valentin on 04/12/14.
 */
trait AbstractHandler {
  def process : (HttpResponse)
}
