package handlers

import helpers.auth.SimpleAuthManager
import schema.MasterKey
import spray.http.{HttpResponse, StatusCodes}
import org.squeryl.PrimitiveTypeMode._
/**
  * Created by Valentin on 07/04/2017.
  */
class KeysHandler(authToken : String) extends BaseHandler {
  def process : (HttpResponse) = {
    if (SimpleAuthManager.checkMasterToken(authToken)) {
      transaction {
//        val response = from(MasterKey.myKeys)(s => select(s)).toList
        HttpResponse(StatusCodes.OK, gson.toJson(MasterKey.myKeys.toArray))
      }
    } else {
      HttpResponse(StatusCodes.Unauthorized, "fuck off")
    }
  }
}