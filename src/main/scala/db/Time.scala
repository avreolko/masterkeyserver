package db

import java.sql.Timestamp

class TimestampRich(val timestamp: Timestamp) {
  def before(period: Long) = after(-period)

  def after(periodMs: Long) = {
    new java.sql.Timestamp(timestamp.getTime + periodMs)
  }
}

object Time {
  val SEC = 1000L
  val MIN = SEC * 60L
  val HOUR = MIN * 60L
  val ONE_DAY = 24L * HOUR

  implicit def timestampToTimestampRich(timestamp: Timestamp) = new TimestampRich(timestamp)

  val Beginning = new Timestamp(0)

  def now = {
    val date = new java.util.Date()
    new Timestamp(date.getTime)
  }

  def before(period: Long) = after(-period)

  def after(period: Long) = {
    val now = new java.util.Date()
    new java.sql.Timestamp(now.getTime + period)
  }
}
