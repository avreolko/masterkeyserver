package schema

import org.squeryl.PrimitiveTypeMode._
import org.squeryl.Schema

/**
  * Created by Valentin on 26/01/17.
  */
object MasterKey extends Schema {
  val myKeys = table[MyKey]
  val settings = table[Settings]

  on(myKeys)(k => declare(
    k.id is (unique,indexed,autoIncremented,primaryKey),
    k.name is indexed
  ))

  on(settings)(u => declare(
    u.id is (unique,indexed,autoIncremented,primaryKey),
    u.entity is indexed
  ))
}
