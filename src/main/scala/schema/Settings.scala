package schema

import db.Time
import org.squeryl.KeyedEntity

/**
  * Created by Valentin on 26/01/17.
  */

class Settings(var entity : String, var value : String) extends KeyedEntity[Long] {
  var id : Long = 0L
}
