package com

import akka.actor.Actor
import handlers._
import spray.http.HttpCookie
import spray.routing._

// we don't implement our route structure directly in the service actor because
// we want to be able to test it independently, without having to spin up an actor
class MyServiceActor extends Actor with MainService {

  // the HttpService trait defines only one abstract member, which
  // connects the services environment to the enclosing actor or test
  def actorRefFactory = context

  // this actor only runs our route, but you could add
  // other things here, like request stream processing
  // or timeout handling
  def receive = runRoute(mainRoute)
}

// this trait defines our service behavior independently from the service actor
trait MainService extends HttpService {
  val mainRoute = {
    get {
      path ("") { complete { "ops" } } ~
      path ("hey") { cookie("authToken") { token => complete { "hey" } } } ~
      path ("keys") { cookie("authToken") { token => complete { new KeysHandler(token.content).process } } } ~
      path ("keys") { complete { new KeysHandler("test").process } } ~
      path ("hello" / IntNumber ) { number => complete { new TestHandler(number).process } } ~
      pathPrefix ("images") { getFromDirectory("images/") }
    } ~
    post {
      path("auth") { entity(as[String]) { json => complete(new AuthHandler(json).process) } }
    }
  }
}