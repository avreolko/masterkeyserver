package com

import akka.actor.{ActorSystem, Props}
import akka.io.IO
import spray.can.Http
import akka.pattern.ask
import akka.util.Timeout
import db.BasicSession
import db.DBSession
import schema.MasterKey

import scala.concurrent.duration._
import scala.util.Try

object Server {

  def main(args: Array[String]) {
    DBSession.configure("db.json")

    if (args.count(_ == "-createDB") == 1) {
      createSchema()
      System.exit(1)
      return
    }

    // we need an ActorSystem to host our application in
    implicit val system = ActorSystem("on-spray-can")

    // create and start our service actor
    val service = system.actorOf(Props[MyServiceActor], "demo-service")

    implicit val timeout = Timeout(5.seconds)
    // start a new HTTP server on port 8080 with our service actor as the handler
    IO(Http) ? Http.Bind(service, interface = "0.0.0.0", port = 10030)
  }

  def createSchema() {
    Try(new BasicSession().transaction(MasterKey.drop))
    Try(new BasicSession().transaction(MasterKey.create))
  }
}
