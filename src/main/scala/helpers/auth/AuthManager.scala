package helpers.auth

/**
  * Created by Alpex on 05.08.2016.
  */
trait AuthManager {

  def check(authToken : String): Boolean

  def addToken(authToken : String, userId : Long): Unit

  def removeToken(authToken : String)

  def getUserIdByToken(authToken : String): Long

  def addMasterToken(authToken : String)

  def checkMasterToken(authToken : String): Boolean

}
