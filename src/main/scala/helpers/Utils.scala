package helpers

import java.security.MessageDigest

import scala.util.Try

/**
  * Created by Valentin on 15/07/16.
  */
object Utils {
  def md5(string: String) : String = {
    val m = MessageDigest.getInstance("MD5")
    val b = string.getBytes("UTF-8")
    m.update(b, 0, b.length)
    new java.math.BigInteger(1, m.digest()).toString(16)
  }

  implicit class TryEventually[T](val t: Try[T]) extends AnyVal {
    def eventually[Ignore](effect: => Ignore): Try[T] = {
      val ignoring = (_: Any) => { effect; t }
      t transform (ignoring, ignoring)
    }
  }

}
