organization  := "com.example"

name := "SprayServer"

version := "0.1"

scalaVersion := "2.11.8"

val akkaVersion  = "2.3.15"
val sprayVersion  = "1.3.3"

scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8")

resolvers += "spray repo" at "http://repo.spray.io"

def spray = Seq(
  "io.spray"            %%  "spray-can"     % sprayVersion,
  "io.spray"            %%  "spray-routing" % sprayVersion,
  "io.spray"            %%  "spray-json"    % sprayVersion,
  "io.spray"            %%  "spray-testkit" % sprayVersion  % "test",
  "com.typesafe.akka"   %%  "akka-actor"    % akkaVersion,
  "com.typesafe.akka"   %%  "akka-testkit"  % akkaVersion   % "test",
  "org.specs2"          %%  "specs2-core"   % "2.3.11" % "test"
)

def db = Seq(
  "c3p0"        % "c3p0"                 % "0.9.1.2",
  "mysql"       % "mysql-connector-java" % "5.1.10",
  "org.squeryl" %% "squeryl"             % "0.9.5-7"
)

def gson = Seq(
  "com.google.code.gson" % "gson" % "2.1"
)

libraryDependencies ++= (spray ++ db ++ gson)

Revolver.settings
